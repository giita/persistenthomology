import numpy as np
from itertools import product
from ripser import ripser
from persim import plot_diagrams
import matplotlib.pyplot as plt


def cube_with_centers(n: int):
    data = list(map(list, product([0.5, -0.5], repeat=n)))
    if n > 1:
        for i in range(n):
            for j in [-0.5, 0.5]:
                p = [0] * n
                p[i] = j
                data.append(p)
    return np.array(data)


def cubes(max_n: int):
    data = np.empty((0, max_n))
    for i in range(2, max_n + 1):
        cube = cube_with_centers(i)
        cube = np.c_[cube, np.zeros((cube.shape[0], max_n-i))]
        data = np.r_[data, cube * 1.1**(i-2) + 5*(i-2)]
    return data


data = cubes(6)

print(data.shape)
print(data.tolist())

plt.subplot(1, 2, 1)
plt.scatter(data.transpose()[0], data.transpose()[1])
plt.axis('equal')
plt.title('Projection of the points on first two coordinates')

#diagrams = ripser(data, maxdim=5, thresh=3)['dgms']
# precomputed, uncomment the line above and remove import below
# to run computation again
from precomputed import diagrams
print(diagrams)
plot_diagrams(diagrams, ax=plt.subplot(1, 2, 2))
plt.axis('equal')
plt.title('Persistent homologies')

plt.show()
