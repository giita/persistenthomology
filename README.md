# Idea

Basic object: n-dimensional cube's (n-1)-dimensional surface. It has one 0-homology and one (n-1)-homology. Here I generate the following points on the surface: one for each vertex of the cube, and also one in the center of each (n-1)-dimensional side. The later class of points is needed so that the (n-1)-homology is born earlier and lives longer.

I generate 5 such surfaces, from dimension 1 to 5, in 6-dimensional space on some distance from one another. This gives me all the needed homologies, including 5 persistent 0-homologies.

I also vary them in size a little so all the interesting homologies don't end up in one point on the diagram (so all can be clearly seen).

The time of simulation is limited so that each of the cubes are fully filled and after that no more connections are made. This emulates independence of the objects and speeds up computation since after cubes are filled nothing interesting happens.

# How to run

* Install dependencies from `requirements-buildenv.txt`:
  `pip install -r requirements-buildenv.txt`

* Install dependencies from `requirements.txt`:
  `pip install -r requirements.txt`

* Install `tk` so that `matplotlib` can show a nice window.

* Run `python h5.py`. Note that this shows precomputed lifetimes so it should be fast. Read the code for directions to rerun the computations. Be warned that it takes ~20 minutes to compute (on my machine) and uses ~7 GiB of RAM.

# Result

![result](result.png)

# Result analysis

* 0-homologies: there are a lot of them, which is expected since many connected components are born at moment 0 and they die off eventually. 5 0-homologies representing the 5 cubes live to the end (threshold), which can't be seen on the picture (they are the same dot) but can be checked in `precomputed.py`.

* 1-homologies: we see two of these. Actually only the earlier one is produced by square and is interesting. The other one is many dots which are garbage produced by 6-dimentional cube during their build-up. Interestingly 3-5 dimensional cubes do not generate these.

* Homologies starting with second are similar and clearly visible. Notice 1-homology from square is a bit longer-lived. I guess it lives so long because it's a special case.
